package com.example.noticias.Clases;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Sevicios {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistrarUsuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<LoginNoticias> getLoginPractica(@Field("username") String correo, @Field("password") String contrasenia);

    @GET("api/todasNot")
    Call<Noticias> NOTICIAS_CALL();
}
