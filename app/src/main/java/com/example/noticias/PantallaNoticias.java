package com.example.noticias;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticias.Clases.Api;
import com.example.noticias.Clases.Detalle;
import com.example.noticias.Clases.Noticias;
import com.example.noticias.Clases.Sevicios;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaNoticias extends AppCompatActivity {

    Button obtener;
    EditText panel;
    List<Detalle> detallelist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_noticias);
        obtener = (Button) findViewById(R.id.obtener);
        panel = (EditText) findViewById(R.id.panel);
        obtener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sevicios service = Api.getApi(PantallaNoticias.this).create(Sevicios.class);
                Call<Noticias> noticias =  service.NOTICIAS_CALL();
                noticias.enqueue(new Callback<Noticias>() {
                    @Override
                    public void onResponse(Call<Noticias> call, Response<Noticias> response) {
                        Noticias peticion = response.body();
                        if (response.isSuccessful())
                            Toast.makeText(PantallaNoticias.this, "hecho", Toast.LENGTH_LONG).show();
                        if(peticion.estado == "true"){
                            panel.setText(String.valueOf(peticion.detalle.get(0).descripcion));
                            Toast.makeText(PantallaNoticias.this, "Petición siendo procesada", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(PantallaNoticias.this, "Petición no procesada", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Noticias> call, Throwable t) {
                        t.printStackTrace();
                        panel.setText(t.getMessage());
                    }
                });
            }
        });
    }
}
