package com.example.noticias;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticias.Clases.Api;
import com.example.noticias.Clases.LoginNoticias;
import com.example.noticias.Clases.Sevicios;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView textView5;
    EditText correo, contrasena;
    Button login;
    public String APITOKEN = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if(token != ""){
            Toast.makeText(MainActivity.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, PantallaNoticias.class));
        }

        correo = (EditText) findViewById(R.id.correo);
        contrasena = (EditText) findViewById(R.id.contrasena);
        login = (Button) findViewById(R.id.login);
        textView5 = (TextView) findViewById(R.id.textView5);

        textView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PantallaRegistrar.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!correo.getText().toString().isEmpty() && !contrasena.getText().toString().isEmpty()){
                    Sevicios service = Api.getApi(MainActivity.this).create(Sevicios.class);
                    Call<LoginNoticias> loginCall =  service.getLoginPractica(correo.getText().toString(),contrasena.getText().toString());
                    loginCall.enqueue(new Callback<LoginNoticias>() {
                        @Override
                        public void onResponse(Call<LoginNoticias> call, Response<LoginNoticias> response) {
                            LoginNoticias peticion = response.body();
                            if(peticion.estado == "true"){
                                APITOKEN = peticion.getToken();
                                guardarPreferencias();
                                Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(MainActivity.this, PantallaNoticias.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginNoticias> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else{
                    Toast.makeText(MainActivity.this, "Llene campos please", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}
